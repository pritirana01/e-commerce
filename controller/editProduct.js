
const express = require('express');
const {Router} = require('express')
const productDetailModel=require('../modal/productDetail')
const getAuthAdmin=require('../middleware/adminAuth')
const router=Router()

router.use(express.json());

router.post("/editProduct",getAuthAdmin, async (req, res) => {
    const { productId, newTitle, newDescription, newPrice } = req.body;
    try {
        

        // Use the model to perform database operations
        const updateResult = await productDetailModel.updateOne(
            { productId },
            {
                $set: {
                    title: newTitle,
                    description: newDescription,
                    price: newPrice
                }
            }
        );

        console.log(updateResult);
        res.status(200).json({ message: "Product details updated successfully" });
    } catch (error) {
        res.status(500).json({ message: "Internal server error", error: error.message });
    }
});
module.exports=router
