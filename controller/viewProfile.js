const express=require('express')
const {Router}=require('express')
const UserModel = require('../modal/userDetail')


const router=Router()
router.post('/viewProfile',async(req,res)=>{
    const {email}=req.body
    try{
         const getProfile=await UserModel.findOne({email:email})
         if(!getProfile){
            res.status(403).json({message:"details not found realted "})
         }
         else{
            res.status(200).json(getProfile)
         }
    }
    catch{
          res.status(400).json({message:"internal server error"})
    }
}

)
module.exports=router