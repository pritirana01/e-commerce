const express=require('express')
const {Router}=require('express')
const productDetail=require('../modal/productDetail')
const userAuthToken=require('../middleware/userAuth')
const router=Router()
router.post('/products',userAuthToken,async(req,res)=>{
   
    try{
        const {title}=req.body
     const getProducts=await productDetail.find({title})
    console.log(getProducts)
     if(!getProducts){
        res.status(400).json({message:"But did'nt any thing found your choice"})
     }
     else{

        res.status(200).json(getProducts)
     }
    }
    catch(error){
                 res.status(400).json({message:'internal server error',error:error.message})
    }
})

module.exports=router