const express=require('express')
const {Router}=require('express')
const productDetail=require('../modal/productDetail')
const router=Router()
router.post('/singleProduct',async(req,res)=>{
    const {productId}=req.body
    try{
         const getProductId=await productDetail.findOne({productId:productId})
         if (!getProductId){
            res.status(403).json({message:"product details not found"})
         }
         else{
            res.status(200).json(getProductId)
         }
    }
    catch(error){
        res.status(400).json({message:"internal server error"})


    }
})
module.exports=router