const express=require('express')
const usermodel=require('../modal/userDetail')
const {Router}=require('express')
const jwt=require('jsonwebtoken')
const secretKey='pari123@'
const router=Router()
router.use(express.json())
router.post('/userLogin',async(req,res)=>{
    const {email,password}=req.body
    try{
        const getLog=await usermodel.findOne({email:email,password:password})
        if(!getLog){
            res.status(400).json({message:"user details not found"})
        }
        else{
            const token=jwt.sign({email:usermodel.email},secretKey,{expiresIn:'5h'})
            res.status(200).json({message:"user verification successfull",token})
        }
    }
    catch(error){
        res.status(400).json({message:"some error occured",error})
    }
})
module.exports=router