const express=require('express')
const {Router}=require('express')
const getCartModal=require('../modal/cartDetail')
const router=Router()
router.post('/addcart',async(req,res)=>{
    const {email,productId}=req.body
    try{
        const addProduct=getCartModal({
            email,
            productId
        })
        if(!addProduct){
            res.status(400).json({message:"product id incorrect"})

        }
        else{
            await addProduct.save()
            res.status(200).json({message:"product added to cart"})
        }
        
    }
    catch(error){
        res.status(400).json({message:"something went wrong"})

    }
})
module.exports=router