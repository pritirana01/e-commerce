const express=require('express')
const {Router}=require('express')
const getauthAdim=require('../middleware/adminAuth')
const productDetailModel=require('../modal/productDetail')
const router=Router()
router.get('/viewProduct',getauthAdim,async(req,res)=>{
    try{
        const showProduct=await productDetailModel.find()
        res.status(200).json(showProduct)
    }
  catch(error){
    res.status(200).json({message:"some error occured",error:error.message})
  }
    

})
module.exports=router