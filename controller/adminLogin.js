const express=require('express')
const {Router}=require('express')
const secretKey="pri123@"
const router=Router()
const getAdminModel=require('../modal/adminDetail')
const jwt=require('jsonwebtoken')
router.use(express.json())
router.post('/adminLogin',async(req,res)=>{
    const {email,password}=req.body
    try{
      const getData= await getAdminModel.findOne({email:email,password:password})
      if(!getData){
        res.json({message:"details are wrong"})
      }
      else{
        const token=jwt.sign({email:getAdminModel.email},secretKey,{expiresIn:"5h"})
        res.json({message:"login sucessfull",token})
      }
     
    }
    catch(error){
          res.json({message:"internal server error",error})
    }
})

module.exports=router