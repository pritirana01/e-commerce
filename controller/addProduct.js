const express=require('express')
const {Router}=require('express')
const productModel=require('../modal/productDetail')
const getauthAdim=require('../middleware/adminAuth')
const router=Router()
router.use(express.json())
router.post('/addProduct',getauthAdim,async(req,res)=>{
    const {title,description,image,price,stock}=req.body
    if (!title || !description || !image || !price || !stock){
        res.status(400).json({message:"some filed is  missed"})
    }
    else{
        try{
            const newProduct =new productModel({
               title,
               description,
               image,
               price,
               stock
   
            })
            await newProduct.save()
            res.status(200).json({message:"data of new product inserted successfully"})
       }
       catch(error){
           res.status(400).json({message:"internal server error",error:error.message})
   
       }
    }
  
})
module.exports=router