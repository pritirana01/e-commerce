const express = require('express');
const { Router } = require('express');
const userDetailsModel = require('../modal/userDetail');
const userSchema = require('../validator/userValidator'); // Import Joi schema
const router = Router();

router.post('/userSignup', async (req, res) => {
    const { name, email, password, contact, address } = req.body;

    try {
        await userSchema.validateAsync(req.body);
    } catch (error) {
        return res.status(400).json({ message: error.message });
    }

    try {
        const newUser = new userDetailsModel({
            name,
            email,
            password,
            contact,
            address
        });

        await newUser.save();
        res.status(201).json({ message: "User details inserted successfully" });
    } catch (error) {
        res.status(500).json({ message: "Internal server error", error: error.message });
    }
});

module.exports = router;
