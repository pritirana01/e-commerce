const express=require('express')
const {Router}=require('express')
const orderModal=require('../modal/orderDetail')
const router=Router()
router.post('/order',async(req,res)=>{
    const {email,productId}=req.body
    try{
              const newOrder=orderModal({
                email,
                productId

              })
              if(!newOrder){
                res.status(403).json({message:"detail not found"})
              }
              else{
                await newOrder.save();
                res.status(200).json({message:"new order details inserted successfully"})
              }
    }
    catch(error){
        res.status(400).json({message:"internal server error"})

    }
})
module.exports=router