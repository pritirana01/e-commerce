const express=require('express')
const {Router}=require('express')
//const app=express()
const getCart=require('../modal/cartDetail')
const getProductDetail=require('../modal/productDetail')
const router=Router()
router.use(express.json())
router.post('/vewCart',async(req,res)=>{
    const { Email_Address } = req.body;
      
        try {
        
          const getEmail = await getCart.find({ Email_Address });
     
      
      
          const productIds = getEmail.map(product => product.productId);
         
      
        
          const productDetails = await getProductDetail.find({ productId: { $in: productIds } });
          res.status(200).json(productDetails)
        
 
      //  console.log(productDetails)
     
         
      
         
        } catch (error) {
          console.error(error);
          res.status(400).json({ message: 'Bad request', error: error.message });
        }
      });
      module.exports=router
/*(app.listen(3000,'localhost',()=>{
    console.log("for 3000")
})*/