const express=require('express')
const {Router}=require('express')
const router=Router()
const getAdminModel=require('../modal/adminDetail')
router.use(express.json()) 
router.use('/adminSignup',async(req,res)=>{
    const {name,email,password,contact}=req.body
    if(!name || !email || !password || !contact){
       res.json({message:"some field data missed"})
    }
    else{
        try{
            const admin=new getAdminModel({
                name,
                email,
                password,
                contact
            })
            const checkData=await getAdminModel.findOne({email:admin.email})
            if(checkData){
                res.json({message:"admin details already present "})
            }
           
            await admin.save()
            res.json({message:"admin data inserted successfully"})

    }
   
      catch(error){
        res.json({message:"internal server error",error:error.message})
    }
}
})
module.exports=router