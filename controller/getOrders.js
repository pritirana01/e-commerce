const express=require('express')
const {Router}=require('express')
const getOrderList=require('../modal/orderDetail')
const router=Router()
router.post('/orderList',async(req,res)=>{
    try{
        const orders=await getOrderList.find()
       if(!orders){
        res.status(403).json({message:"no order found"})
       }
       else{
        res.status(200).json(orders)
       }
    }
    catch(error){
        res.status(400).json({message:"internal server error"})
    }
})
module.exports=router