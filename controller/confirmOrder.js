const express=require('express')
const {Router}=require('express')
const getOrder=require('../modal/orderDetail')
const router=Router()
router.post('/confirmOrder',async(req,res)=>{
    const {productId}=req.body
    try{
       const doneOrder=await getOrder.findOne({productId:productId})
       if(doneOrder){
        await getOrder({
            status:"order confirmed"
        })
     
       }
       else{
        res.status(403).json({message:"product not found"})
       }
    }
    catch(error){
          res.status(400).json({message:"internal server error",error:error.message})
    }
})

module.exports=router