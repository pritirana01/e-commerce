const joi=require('joi')
const userSchema=joi.object({
    name:joi.string().required(),
    email:joi.string().email().required(),
    password:joi.string().required(),
    contact:joi.number().required(),
    address:joi.string().required()

})

//const userDetails=mongoose.model('userDetails',userSchema)
module.exports=userSchema