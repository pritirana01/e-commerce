const express=require('express')
const app=express()
const getAdminRouter=require('./routes/adminDetail')
const getAdminLoginRouter=require('./routes/adminLogin')
const getAdminToken=require('./middleware/adminAuth')
const getaddProduct=require('./routes/addProduct')
const getViewProduct=require('./routes/viewProduct')
const getEditProduct=require('./controller/editProduct')
const getViewprofile=require('./controller/viewProfile')
const getUserDetail=require('./routes/userDetails')
const getUserLogin=require('./routes/userLogin')
const getUserAuth=require('./middleware/userAuth')
const getproducts=require('./routes/products')
const getSingleProduct=require('./routes/singleProduct')
const getAddCartRouter=require('./routes/addCart')

app.use(getAdminRouter)

app.use(getAdminLoginRouter)
app.use('/admin',getAdminToken)

app.use('/admin',getaddProduct)
app.use('/admin',getViewProduct)
app.use('/admin',getEditProduct)
app.use('/admin',getViewprofile)
app.use(getUserDetail)
app.use(getUserLogin)
app.use('/user',getUserAuth)
app.use('/user',getproducts)
app.use('/user',getSingleProduct)
app.use('/user',getAddCartRouter)


app.listen(4000,'localhost',()=>{
    console.log("server running on port number 4000")
})