const jwt=require('jsonwebtoken')
const secretKey="pri123@"
function authAdim(req,res,next){
        const token=req.header('Authorization')
        if(!token){
            res.status(400).json({message:"token not found"})
        }
        else{
            jwt.verify(token,secretKey,(error,decoded)=>{
              if(error){
                res.status(403).json({message:"token forbidden"})
              }
              else{
                req.user=decoded.email
                next()
              }

            })
        }
}
module.exports=authAdim