const mongoose=require('mongoose')
mongoose.connect('mongodb://127.0.0.1:27017/eCommerce')
.then(()=>{
    console.log("connection done")
})
.catch(()=>{
    console.log("some error occured")
})
const adminSchema= new mongoose.Schema({
    name:{
        type:String,
        required:true
    },
    email:{
        type:String,
        required:true,
        validate:{
            validator:function (value){
                return /^[^\s@]+@gmail\.com$/.test(value)
            },
            message:'insert email address in correct format'}

    },
    password:{
        type:String,
        required:true,
       /* validate:{
            validator:function(value){
                return /[a-zA-Z0-9]{3,30}$/.test(value)
            },
            message:"password should be combo of special letter,capital alphabet,small alphabet "
        }

    }*/
},
contact:{
    type:Number,
    validate:{
        validator: function(value){
            return /^\d{10}$/.test(value)
        }
    }
}
    

})
const adminModel =mongoose.model('adminModel',adminSchema)
module.exports=adminModel