const mongoose = require('mongoose');
//const userSchema = require('../validator/userValidator'); // Import Joi schema
mongoose.connect('mongodb://127.0.0.1:27017/eCommerce')

// Define the Mongoose schema
const UserSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        unique: true,
        required: true,
      
    },
    password: {
        type: String,
        required: true
    },
    contact: {
        type: Number,
        required: true
    },
    address: {
        type: String,
        required: true
    },
  });
const UserModel = mongoose.model('User', UserSchema);
module.exports = UserModel;
