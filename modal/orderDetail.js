const mongoose=require('mongoose')

const { v4: uuidv4 } = require('uuid');
mongoose.connect('mongodb://127.0.0.1:27017/eCommerce')
const orderSchema=new mongoose.schema({
    orderNo:{
        type:String,
        default: uuidv4,
        required: true

    },
    email:{
        type:String,
        required:true
    },
    productId:{
        type:String,
        required:true
    }
})
const order=mongoose.model('order',orderSchema)
module.exports=order