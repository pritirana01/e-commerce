const mongoose=require('mongoose')
mongoose.connect('mongodb://127.0.0.1:27017/eCommerce')
const cartSchema=new mongoose.Schema({
    productId:{
        type:String,
        required:true
    }
})
const cart=mongoose.model('cart',cartSchema)
module.exports=cart