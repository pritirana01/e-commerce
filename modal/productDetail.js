const mongoose = require('mongoose');
const { v4: uuidv4 } = require('uuid');
//const Joi = require('joi');

// Define Mongoose schema without joi
const productSchema = new mongoose.Schema({
    productId: {
        type: String,
        default: uuidv4,
        required: true
    },
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    image:{
        data:Buffer,
        contentType:String
        
    },
    price: {
        type: Number,
        required: true
    },
    stock:{
       type: Number,
       required:true

    }
  
    

});

const ProductDetail = mongoose.model('ProductDetail', productSchema);
module.exports=ProductDetail



