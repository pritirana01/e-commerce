const express=require('express')
const {Router}=require('express')
const productDetailModel=require('../controller/viewProduct')
const router=Router()
router.use(productDetailModel)
module.exports=router