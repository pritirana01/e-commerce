const express=require('express')
const {Router}=require('express')
const getAddCart=require('../controller/addCart')
const router=Router()
router.use(getAddCart)
module.exports=router