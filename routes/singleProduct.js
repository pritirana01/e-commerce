const express=require('express')
const {Router}=require('express')
const router=Router()
const singleProductRouter=require('../controller/singleProduct')
router.use(singleProductRouter)
module.exports=router