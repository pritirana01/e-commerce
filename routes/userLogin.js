const express=require('express')
const {Router}=require('express')
const userLogin=require('../controller/userLogin')
const router=Router()
router.use(userLogin)
module.exports=router