const express=require('express')
const {Router}=require('express')
const products=require('../controller/products')
const router=Router(products)
router.use(products)
module.exports=router