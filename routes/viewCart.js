const express=require('express')
const {Router}=require('express')
const getViewCart=require('../controller/viewCart')
const router=Router()
router.use(getViewCart)
module.exports=router