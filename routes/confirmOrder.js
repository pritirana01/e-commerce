const express=require('express')
const {Router}=require('express')
const getConfirmOrder=require('../controller/confirmOrder')
const router=Router()
router.use(getConfirmOrder)
module.exports=router