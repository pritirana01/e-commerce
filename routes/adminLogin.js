const express=require("express")
const {Router}=require('express')
const getAdminLogin=require('../controller/adminLogin')
const router=Router()
router.use(getAdminLogin)

module.exports=router