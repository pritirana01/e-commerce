const express=require('express')
const {Router}=require('express')
const getOrder=require('../controller/order')
const router=Router()
router.use(getOrder)
module.exports=router