const express=require('express')
const {Router}=require('express')
const addProduct=require('../controller/addProduct')
const router=Router()
router.use(addProduct)
module.exports=router