const express=require('express')
const {Router}=require('express')
const router=Router()
const adminRouter=require('../controller/adminDetail')
router.use(adminRouter)
module.exports=router
