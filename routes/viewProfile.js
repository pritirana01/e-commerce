const express=require('express')
const {Router}=require('express')
const userDetail=require('../controller/viewProfile')
const router=Router()
router.use(userDetail)
module.exports=router