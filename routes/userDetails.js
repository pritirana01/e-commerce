const express=require('express')
const {Router}=require('express')
const userDetailsModel=require('../controller/userDetails')
const router=Router(userDetailsModel)
router.use(userDetailsModel)
module.exports=router
